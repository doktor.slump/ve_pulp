// Import Modules
import { VEPulpActor } from "./actor/actor.js";
import { VEPulpActorSheet } from "./actor/actor-sheet.js";
import { VEPulpItem } from "./item/item.js";
import { VEPulpItemSheet } from "./item/item-sheet.js";
import { VEPulpTalentSheet } from "./item/talent-sheet.js";
import { VEPulpWeaponSheet } from "./item/weapon-sheet.js";

import * as dice from "./dice.js"

Hooks.once('init', async function() {

game.ve_pulp = {
   VEPulpActor,
   VEPulpItem,
   dice:dice
  };

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "@abilities.dex.value",
    decimals: 2
  };

  // Define custom Entity classes
  CONFIG.Actor.entityClass = VEPulpActor;
  CONFIG.Item.entityClass = VEPulpItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("ve_pulp", VEPulpActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("ve_pulp", VEPulpItemSheet, { makeDefault: true });
  Items.registerSheet("ve_pulp", VEPulpTalentSheet, { makeDefault: true });
  Items.registerSheet("ve_pulp", VEPulpWeaponSheet, { makeDefault: true });
  // // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });
  Handlebars.registerHelper('toUpperCase', function(str) {
    return str.toUpperCase();
  });
  Handlebars.registerHelper('strip', function(str) {
    return str.replace(/<\/?[^>]+(>|$)/g, "");
  });   
});
