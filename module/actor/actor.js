/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class VEPulpActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;

    // Make modifications to data here. For example:

    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      // Calculate the modifier using d20 rules.
      if (ability.value == 3) {
        ability.mod = -2;
      }
      else if (ability.value>=4 && ability.value<=6){
        ability.mod = -1;
      }
      else if (ability.value>=7 && ability.value<=14){
        ability.mod = 0;
      }
      else if (ability.value>=15 && ability.value<=17){
        ability.mod= 1;
      }
      else if (ability.value==18){
        ability.mod= 2;
      }


    }
    data.details.level.value=(Math.trunc(data.details.xp.value/10)+1);
    data.details.level.value>5?data.details.level.value=5:data.details.level.value;
    data.attributes.ins.value = data.classes[data.class.value].lv[data.details.level.value-1].ins;
    data.attributes.pulp.max = data.classes[data.class.value].lv[data.details.level.value-1].pulp;
    if (data.details.level.value==1){
      data.attributes.hp.max= parseInt(data.classes[data.class.value].DA.substr(1))+data.abilities.con.mod;

    };
    data.attributes.atq.value=data.classes[data.class.value].lv[data.details.level.value-1].att;
  }
}
