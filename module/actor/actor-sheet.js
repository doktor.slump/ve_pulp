import { d20Roll,damageRoll } from "../dice.js";

/**
* Extend the basic ActorSheet with some very simple modifications
* @extends {ActorSheet}
*/
export class VEPulpActorSheet extends ActorSheet {
  
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["ve_pulp", "sheet", "actor"],
      template: "systems/ve_pulp/templates/actor/actor-sheet.html",
      width: 1040,
      height: 764,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }
  
  
  
  
  
  /* -------------------------------------------- */
  
  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];
    for (let attr of Object.values(data.data.attributes)) {
      attr.isCheckbox = attr.dtype === "Boolean";
    }
    //Prepare Items
    if (this.actor.data.type == 'character'){
      this._prepareCharacterItems(data);
    }
    
    return data;
  };
  
  
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;
    const talent = [];
    const weapon = [];
    const gear = [];
    for (let i of sheetData.items) {
      let item = i.data;
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'talent') {
        talent.push(i);
      }
      // Append to features.
      else if (i.type === 'weapon') {
        weapon.push(i);
      }
      // Append to spells.
      else if (i.type === 'item') {
        gear.push(i);
      }
    }
    
    // Assign and return
    actorData.talent = talent;
    actorData.weapon = weapon;
    actorData.item = gear;
    console.log(actorData.weapon);
  };
  
  
  
  
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    
    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;
    
    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));
    
    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });
    
    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });
    
    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.combat').click(this._damageRoll.bind(this));
    // html.find('atq').click(this._atq.bind(this));
    html.find('.atq').click(this._atq.bind(this));
  }
  
  /* -------------------------------------------- */
  
  /**
  * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
  * @param {Event} event   The originating click event
  * @private
  */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];
    
    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }
  
  /**
  * Handle clickable rolls.
  * @param {Event} event   The originating click event
  * @private
  */
  _onRoll(event) {
    
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let label = dataset.label ? `Rolling ${dataset.label}` : '';
    

    if (dataset.roll){
      d20Roll({rollMode:"CHAT.RollPublic",parts:[dataset.roll],data:this.actor.data.data,flavor:label,speaker: ChatMessage.getSpeaker({ actor: this.actor }),critical:20,fumble:1});
    }
  };
  _damageRoll(event) {
    
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;  
    let label = dataset.label ? `Rolling ${dataset.label}` : '';
    if (dataset.roll){
      damageRoll({rollMode:"CHAT.RollPublic",parts:[dataset.roll],data:this.actor.data.data,flavor:label,speaker: ChatMessage.getSpeaker({ actor: this.actor })});
    };
  }
  _atq(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;  
    let label = dataset.label ? `Rolling ${dataset.label}` : '';
  
    let d = new Dialog({
      title: "Ataque",
      content: "<p>Tipo de ataque</p>",
      buttons: {
        dist: {
          icon: '<i class="fas fa-check"></i>',
          label: "Distancia",
          callback: () => d20Roll({rollMode:"CHAT.RollPublic",parts:[dataset.roll+"+@abilities.dex.mod"],data:this.actor.data.data,flavor:label,speaker: ChatMessage.getSpeaker({ actor: this.actor })})
        },
        cac: {
          icon: '<i class="fas fa-times"></i>',
          label: "Cuerpo a Cuerpo",
          callback: () => d20Roll({rollMode:"CHAT.RollPublic",parts:[dataset.roll+"+@abilities.str.mod"],data:this.actor.data.data,flavor:label,speaker: ChatMessage.getSpeaker({ actor: this.actor })})
        }
      },
      default: "cac",
      close: () => console.log("This always is logged no matter which option is chosen")
    });
    d.render(true);
    
  }
  
}
