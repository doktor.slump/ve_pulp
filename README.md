<!--
 Copyright (c) 2020 Dr. Slump
 
 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

Esta es una implementación inicial de Vieja Escuela pulp segunda edición ( https://grapasymapas.com/pulp ) para Foundry VTT.
- Incluye las diferentes clases de personajes con sus habilidades iniciales calculadas. 
- Calculo de nivel y características basados en clase por experiencia (10pe = 1 nivel, máximo nivel 5) .
- Un compendio con los posibles talentos para arrastrarlos directamente a la hoja de personaje. 
- Lanzamientos de tiradas de instintos.
- Tiradas tanto de habilidades como características con ventaja, desventaja y modificadores.
- Tiradas de daño normal o crítico. (Max. daño+1)




